﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StringProject
{
    public static class UniqueString
    {
        public static bool CheckStringForUnique(this string str1)
        {
            if (str1.Length == 0)
            {
                Console.WriteLine("The string is empty");
                return false;
            }
            if (str1.Length > 256)
            {
                Console.WriteLine("The string is too larg");
                return false;
            }
            Boolean[] value = new Boolean[256];
            for (int i = 0; i < str1.Length; i++)
            {
                int val = str1[i];
                if (value[val])
                {
                    Console.WriteLine("The string has not unique character");
                    return false;
                }
                value[val] = true;
            }
            Console.WriteLine("The string has unique character");
            return true;
        }

    }
}
