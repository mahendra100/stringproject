﻿using System;

namespace StringProject
{
    class Program
    {
        static void Main(string[] args)
        {
            bool result;
            Console.WriteLine("Enter 1 for Unique ");
            Console.WriteLine("Enter 2 for Permutation of other string ");
            Console.WriteLine("Enter 3 for replace all spaces in a string with '%20");
            Console.WriteLine("Enter 4 Check Edit one or edit zero");

            var op = Console.ReadLine();

            switch (op)
            {
                case "1":
                    Console.WriteLine("Enter String");
                    string str1 = Console.ReadLine();
                    result = str1.CheckStringForUnique();
                    Console.ReadLine();
                    break;

                case "2":
                    Console.WriteLine("Enter first String");
                    string str2 = Console.ReadLine();
                    Console.WriteLine("Enter second String");
                    string str3 = Console.ReadLine();
                    result = str2.CheckStringForPermutation(str3);
                    Console.ReadLine();
                    break;

                case "3":
                    Console.WriteLine("Enter String");
                    string str4 = Console.ReadLine();
                    result = str4.ReplaceSpaceString();
                    Console.ReadLine();
                    break;

                case "4":
                    Console.WriteLine("Enter first String");
                    string str5 = Console.ReadLine();
                    Console.WriteLine("Enter second String");
                    string str6 = Console.ReadLine();
                    int result1 = str5.CheckNumberOfCharEdit(str6);
                    if (result1 == 1 || result1 == 0)
                    {
                        Console.WriteLine("true");
                    }
                    else
                        Console.WriteLine("false");
                    Console.ReadLine();
                    break;
            }
        }
    }
}
