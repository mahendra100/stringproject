﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StringProject
{
    public static class PermutationString
    {
        public static bool CheckStringForPermutation(this string str1, string str2)
        {
            if (str1.Length != str2.Length)
            {
                Console.WriteLine("Second string is not Permutation of Frist string.");
                return false;
            }
            for (int i = 0; i < str1.Length; i++)
            {
                if (str1.Count(t => t == str2[i]) != str2.Count(t => t == str1[i]))
                {
                    Console.WriteLine("Second string is not Permutation of Frist string.");
                    return false;
                }
            }
            Console.WriteLine("Second string is Permutation of Frist string.");
            return true;
        }
    }
}
