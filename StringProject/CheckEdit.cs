﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StringProject
{
    public static class CheckEdit
    {
        public static int CheckNumberOfCharEdit(this string str1, string str2)
        {
            int len1 = str1.Length, len2 = str2.Length;
            int[,] dp = new int[len1 + 1, len2 + 1];

            for (int i = 0; i <= len1; i++)
            {
                for (int j = 0; j <= len2; j++)
                {
                    if (i == 0)
                        dp[i, j] = j;

                    else if (j == 0)
                        dp[i, j] = i;

                    else if (str1[i - 1] == str2[j - 1])
                        dp[i, j] = dp[i - 1, j - 1];

                    else
                        dp[i, j] = 1 + min(dp[i, j - 1],   
                                        dp[i - 1, j],      
                                        dp[i - 1, j - 1]); 
                }
            }

            return dp[len1, len2];
        }
        static int min(int x, int y, int z)
        {
            if (x <= y && x <= z) return x;
            if (y <= x && y <= z) return y;
            else return z;
        }

    }
}
